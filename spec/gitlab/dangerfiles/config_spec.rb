# frozen_string_literal: true

require "spec_helper"

require "gitlab/dangerfiles/config"

RSpec.describe Gitlab::Dangerfiles::Config do
  subject(:config) { described_class.new }

  describe "#code_size_thresholds" do
    context "with no failures" do
      it { expect(config.code_size_thresholds).to eq(high: 2_000, medium: 500) }
    end
  end

  describe "overriding of config" do
    it "is possible to override a config directly" do
      new_config = {
        high: config.code_size_thresholds[:high] / 2,
        medium: config.code_size_thresholds[:medium] / 2,
      }

      config.code_size_thresholds = new_config

      expect(config.code_size_thresholds).to eq(new_config)
    end
  end
end
