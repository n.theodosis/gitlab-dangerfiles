require "bundler/setup"
require "danger"
require "timecop"
require "webmock"
require "webmock/rspec"
require "climate_control"
require "cork"
require "rspec-parameterized"
require "gitlab/dangerfiles"

Dir[File.expand_path("support/**/*.rb", __dir__)].sort.each { |f| require f }

RSpec.configure do |config|
  config.filter_run focus: true
  config.run_all_when_everything_filtered = true

  # Enable flags like --only-failures and --next-failure
  config.example_status_persistence_file_path = ".rspec_status"

  # Disable RSpec exposing methods globally on `Module` and `main`
  config.disable_monkey_patching!

  config.expect_with :rspec do |c|
    c.syntax = :expect
  end

  config.before(:suite) do
    Timecop.safe_mode = true
  end
end

RSpec.shared_context "with testing dangerfile" do
  include_context "with dangerfile"

  let(:dangerfile) { DangerSpecHelper.testing_dangerfile }
end
