# frozen_string_literal: true

require "spec_helper"

require "danger/plugins/helper"

RSpec.describe Danger::Helper do
  include_context "with testing dangerfile"

  it "is a plugin" do
    expect(described_class < Danger::Plugin).to be_truthy
  end

  let(:mr_author) { nil }
  let(:categories) do
    {
      %r{\Adoc/.*\z} => :docs,
      %r{\Achangelogs/.*\z} => :none,
    }
  end
  let(:diff) { "- foo\n+ bar" }
  let(:fake_git_diff) { double(patch: diff) }

  subject(:helper) { dangerfile.helper }

  before do
    allow(dangerfile.gitlab).to receive(:mr_author).and_return(mr_author)
    allow(dangerfile.gitlab).to receive(:mr_changes).and_return(mr_changes_from_api["changes"])
  end

  describe "#config" do
    context "when running locally" do
      it "returns a Gitlab::Dangerfiles::Config object" do
        expect(helper.config).to be_an(Gitlab::Dangerfiles::Config)
      end
    end

    it "is possible to override the config with a block" do
      new_config = {
        high: helper.config.code_size_thresholds[:high] / 2,
        medium: helper.config.code_size_thresholds[:medium] / 2,
      }

      helper.config do |config|
        config.code_size_thresholds = new_config
      end

      expect(helper.config.code_size_thresholds).to eq(new_config)
    end
  end

  describe "#html_link" do
    let(:text) { "something" }

    context "when running locally" do
      before do
        allow(helper).to receive(:ci?).and_return(false)
      end

      it "returns the same string" do
        expect(dangerfile.gitlab).not_to receive(:html_link)

        expect(helper.html_link(text)).to eq(text)
      end
    end

    context "when running under CI" do
      it "returns a HTML link formatted version of the string" do
        html_formatted_text = %Q{<a href="#{text}">#{text}</a>}

        expect(dangerfile.gitlab).to receive(:html_link).with(text, full_path: true).and_return(html_formatted_text)

        expect(helper.html_link(text)).to eq(html_formatted_text)
      end
    end
  end

  describe "#ci?" do
    context "when gitlab_danger_helper is not available" do
      before do
        allow(helper).to receive(:gitlab_helper).and_return(nil)
      end

      it "returns false" do
        expect(helper.ci?).to be_falsey
      end
    end

    context "when gitlab_danger_helper is available" do
      it "returns true" do
        expect(helper.ci?).to be_truthy
      end
    end
  end

  describe "#gitlab_helper" do
    it "returns the gitlab helper" do
      expect(helper.__send__(:gitlab_helper)).to eq(dangerfile.gitlab)
    end
  end

  describe "#release_automation?" do
    context "when gitlab helper is not available" do
      it "returns false" do
        expect(helper.release_automation?).to be_falsey
      end
    end

    context "when gitlab helper is available" do
      context "but the MR author isn't the RELEASE_TOOLS_BOT" do
        let(:mr_author) { "johnmarston" }

        it "returns false" do
          expect(helper.release_automation?).to be_falsey
        end
      end

      context "and the MR author is the RELEASE_TOOLS_BOT" do
        let(:mr_author) { described_class::RELEASE_TOOLS_BOT }

        it "returns true" do
          expect(helper.release_automation?).to be_truthy
        end
      end
    end
  end

  describe "#added_files" do
    subject { helper.added_files }

    context "with the GitLab API available" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "interprets a list of changes from the GitLab API" do
        is_expected.to contain_exactly("added-from-api")
      end
    end

    context "with the GitLab API unavailable" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(nil)
      end

      it "interprets a list of changes from the danger git plugin" do
        is_expected.to contain_exactly("added-from-git")
      end
    end
  end

  describe "#modified_files" do
    subject { helper.modified_files }

    context "with the GitLab API available" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "interprets a list of changes from the GitLab API" do
        is_expected.to contain_exactly("modified-from-api")
      end
    end

    context "with the GitLab API unavailable" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(nil)
      end

      it "interprets a list of changes from the danger git plugin" do
        is_expected.to contain_exactly("modified-from-git")
      end
    end
  end

  describe "#renamed_files" do
    subject { helper.renamed_files }

    context "with the GitLab API available" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "interprets a list of changes from the GitLab API" do
        is_expected.to eq([{ before: "renamed_before-from-api", after: "renamed_after-from-api" }])
      end
    end

    context "with the GitLab API unavailable" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(nil)
      end

      it "interprets a list of changes from the GitLab API" do
        is_expected.to eq([{ before: "renamed_before-from-git", after: "renamed_after-from-git" }])
      end
    end
  end

  describe "#deleted_files" do
    subject { helper.deleted_files }

    context "with the GitLab API available" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "interprets a list of changes from the GitLab API" do
        is_expected.to contain_exactly("deleted-from-api")
      end
    end

    context "with the GitLab API unavailable" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(nil)
      end

      it "interprets a list of changes from the danger git plugin" do
        is_expected.to contain_exactly("deleted-from-git")
      end
    end
  end

  describe "#all_changed_files" do
    subject { helper.all_changed_files }

    context "on CI" do
      it "interprets a list of changes from the danger git plugin" do
        is_expected.to contain_exactly("added-from-api", "modified-from-api", "renamed_after-from-api")
      end
    end

    context "locally" do
      before do
        allow(helper).to receive(:ci?).and_return(false)
      end

      let(:added_files) { %w[a b c.old] }
      let(:modified_files) { %w[d e] }
      let(:renamed_files) { [{ before: "c.old", after: "c.new" }] }

      it "interprets a list of changes from the danger git plugin" do
        is_expected.to contain_exactly("a", "b", "c.new", "d", "e")
      end
    end
  end

  describe "#changed_lines" do
    subject { helper.changed_lines("added-from-api") }

    context "when running locally" do
      before do
        allow(helper).to receive(:ci?).and_return(false)
        expect(fake_git).to receive(:diff_for_file).with("added-from-api").and_return(fake_git_diff)
      end

      it "returns diff from Git" do
        is_expected.to eq(["- foo", "+ bar"])
      end
    end

    context "when running under CI" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "returns diff from the API" do
        is_expected.to eq(["- vendor/ruby/", "+.danger-review-cache:"])
      end
    end
  end

  describe "#markdown_list" do
    it "creates a markdown list of items" do
      items = %w[a b]

      expect(helper.markdown_list(items)).to eq("* `a`\n* `b`")
    end

    it "wraps items in <details> when there are more than 10 items" do
      items = ("a".."k").to_a

      expect(helper.markdown_list(items)).to match(%r{<details>[^<]+</details>})
    end
  end

  describe "#changes_by_category" do
    context "on CI" do
      it "interprets a list of changes from the danger git plugin" do
        expect(helper.changes_by_category(categories)).to eq(unknown: %w[added-from-api modified-from-api renamed_after-from-api])
      end
    end

    context "locally" do
      before do
        allow(helper).to receive(:ci?).and_return(false)
      end

      let(:added_files) { %w[doc/doc1 foo] }
      let(:modified_files) { %w[changelogs/entry1 doc/doc2] }
      let(:renamed_files) { [{ before: "", after: "doc/doc3" }, { before: "doc/doc4", after: "changelogs/entry2" }] }

      it "categorizes changed files" do
        expect(helper.changes_by_category(categories)).to eq(
          docs: %w[doc/doc1 doc/doc2 doc/doc3],
          none: %w[changelogs/entry1 changelogs/entry2],
          unknown: %w[foo],
        )
      end
    end
  end

  describe "#changes" do
    it "returns an array of Change objects" do
      expect(helper.changes(categories)).to be_an(Gitlab::Dangerfiles::Changes)
    end

    it "groups changes by change type" do
      changes = helper.changes(categories)

      expect(changes.added.files).to eq(["added-from-api"])
      expect(changes.modified.files).to eq(["modified-from-api"])
      expect(changes.deleted.files).to eq(["deleted-from-api"])
      expect(changes.renamed_before.files).to eq(["renamed_before-from-api"])
      expect(changes.renamed_after.files).to eq(["renamed_after-from-api"])
    end
  end

  describe "#categories_for_file" do
    using RSpec::Parameterized::TableSyntax

    where(:path, :expected_categories) do
      "doc/foo" | [:docs]
      "changelogs/foo" | [:none]
      "foo" | [:unknown]
    end

    with_them do
      subject { helper.categories_for_file(path, categories) }

      it { is_expected.to eq(expected_categories) }
    end
  end

  describe "#label_for_category" do
    using RSpec::Parameterized::TableSyntax

    where(:category, :expected_label) do
      :backend | "~backend"
      :database | "~database"
      :docs | "~documentation"
      :foo | "~foo"
      :frontend | "~frontend"
      :none | ""
      :qa | "~QA"
      :engineering_productivity | '~"Engineering Productivity" for CI, Danger' # Deprecated as of 2.3.0 in favor of tooling
      :tooling | "~tooling for CI, Danger"
      :ci_template | '~"ci::templates"'
      :product_intelligence | '~"product intelligence"'
    end

    with_them do
      subject { helper.label_for_category(category) }

      it { is_expected.to eq(expected_label) }
    end
  end

  describe "#mr_iid" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_iid).to eq("")
    end

    it "returns the MR IID when `gitlab_helper` is available" do
      mr_iid = 1234
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("iid" => mr_iid)

      expect(helper.mr_iid).to eq(mr_iid.to_s)
    end
  end

  describe "#mr_author" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_author).to eq(`whoami`.strip)
    end

    it "returns the MR author when `gitlab_helper` is available" do
      mr_author = "1234"
      expect(dangerfile.gitlab).to receive(:mr_author).and_return(mr_author)

      expect(helper.mr_author).to eq(mr_author)
    end
  end

  describe "#mr_title" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_title).to eq("")
    end

    it "returns the MR title when `gitlab_helper` is available" do
      mr_title = "My MR title"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => mr_title)

      expect(helper.mr_title).to eq(mr_title)
    end
  end

  describe "#mr_web_url" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_web_url).to eq("")
    end

    it "returns the MR web_url when `gitlab_helper` is available" do
      mr_web_url = "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("web_url" => mr_web_url)

      expect(helper.mr_web_url).to eq(mr_web_url)
    end
  end

  describe "#mr_labels" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_labels).to eq([])
    end

    it "returns the MR labels when `gitlab_helper` is available" do
      mr_labels = %w[foo bar baz]
      expect(dangerfile.gitlab).to receive(:mr_labels).and_return(mr_labels)

      expect(helper.mr_labels).to eq(mr_labels)
    end
  end

  describe "#mr_source_branch" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_source_branch).to eq(`git rev-parse --abbrev-ref HEAD`.strip)
    end

    it "returns the MR source branch when `gitlab_helper` is available" do
      mr_source_branch = "main"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("source_branch" => mr_source_branch)

      expect(helper.mr_source_branch).to eq(mr_source_branch)
    end
  end

  describe "#mr_target_branch" do
    it 'returns "" when `gitlab_helper` is unavailable' do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_target_branch).to eq("")
    end

    it "returns the MR target branch when `gitlab_helper` is available" do
      mr_target_branch = "main"
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("target_branch" => mr_target_branch)

      expect(helper.mr_target_branch).to eq(mr_target_branch)
    end
  end

  describe "#squash_mr?" do
    it "returns true when `gitlab_helper` is unavailable" do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper).to be_squash_mr
    end

    it "returns true when MR is to set to be squashed" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("squash" => true)

      expect(helper).to be_squash_mr
    end
    it "returns true when MR is to set to be squashed" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("squash" => false)

      expect(helper).not_to be_squash_mr
    end
  end

  describe "#security_mr?" do
    it "returns false when on a normal merge request" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("web_url" => "https://gitlab.com/gitlab-org/gitlab/-/merge_requests/1")

      expect(helper).not_to be_security_mr
    end

    it "returns true when on a security merge request" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("web_url" => "https://gitlab.com/gitlab-org/security/gitlab/-/merge_requests/1")

      expect(helper).to be_security_mr
    end
  end

  describe "#draft_mr?" do
    it "returns true for a draft MR" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("work_in_progress" => true)

      expect(helper).to be_draft_mr
    end

    it "returns false for non draft MR" do
      expect(dangerfile.gitlab).to receive(:mr_json).and_return("work_in_progress" => false)

      expect(helper).not_to be_draft_mr
    end
  end

  describe "#cherry_pick_mr?" do
    context "when MR title does not mention a cherry-pick" do
      it "returns false" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Add feature xyz")

        expect(helper).not_to be_cherry_pick_mr
      end
    end

    context "when MR title mentions a cherry-pick" do
      [
        "Cherry Pick !1234",
        "cherry-pick !1234",
        "CherryPick !1234",
      ].each do |mr_title|
        it "returns true" do
          expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => mr_title)

          expect(helper).to be_cherry_pick_mr
        end
      end
    end
  end

  describe "#run_all_rspec_mr?" do
    context "when MR title does not mention RUN ALL RSPEC" do
      it "returns false" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Add feature xyz")

        expect(helper).not_to be_run_all_rspec_mr
      end
    end

    context "when MR title mentions RUN ALL RSPEC" do
      it "returns true" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Add feature xyz RUN ALL RSPEC")

        expect(helper).to be_run_all_rspec_mr
      end
    end
  end

  describe "#run_as_if_foss_mr?" do
    context "when MR title does not mention RUN AS-IF-FOSS" do
      it "returns false" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Add feature xyz")

        expect(helper).not_to be_run_as_if_foss_mr
      end
    end

    context "when MR title mentions RUN AS-IF-FOSS" do
      it "returns true" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("title" => "Add feature xyz RUN AS-IF-FOSS")

        expect(helper).to be_run_as_if_foss_mr
      end
    end
  end

  describe "#stable_branch?" do
    it "returns false when `gitlab_helper` is unavailable" do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper).not_to be_stable_branch
    end

    context "when MR target branch is not a stable branch" do
      it "returns false" do
        expect(dangerfile.gitlab).to receive(:mr_json).and_return("target_branch" => "my-feature-branch")

        expect(helper).not_to be_stable_branch
      end
    end

    context "when MR target branch is a stable branch" do
      %w[
        13-1-stable-ee
        13-1-stable-ee-patch-1
      ].each do |target_branch|
        it "returns true" do
          expect(dangerfile.gitlab).to receive(:mr_json).and_return("target_branch" => target_branch)

          expect(helper).to be_stable_branch
        end
      end
    end
  end

  describe "#mr_has_label?" do
    it "returns false when `gitlab_helper` is unavailable" do
      expect(helper).to receive(:gitlab_helper).and_return(nil)

      expect(helper.mr_has_labels?("telemetry")).to be_falsey
    end

    context "when mr has labels" do
      before do
        mr_labels = ["telemetry", "telemetry::reviewed"]
        expect(dangerfile.gitlab).to receive(:mr_labels).and_return(mr_labels)
      end

      it "returns true with a matched label" do
        expect(helper.mr_has_labels?("telemetry")).to be_truthy
      end

      it "returns false with unmatched label" do
        expect(helper.mr_has_labels?("database")).to be_falsey
      end

      it "returns true with an array of labels" do
        expect(helper.mr_has_labels?(["telemetry", "telemetry::reviewed"])).to be_truthy
      end

      it "returns true with multi arguments with matched labels" do
        expect(helper.mr_has_labels?("telemetry", "telemetry::reviewed")).to be_truthy
      end

      it "returns false with multi arguments with unmatched labels" do
        expect(helper.mr_has_labels?("telemetry", "telemetry::non existing")).to be_falsey
      end
    end
  end

  describe "#labels_list" do
    let(:labels) { ["telemetry", "telemetry::reviewed"] }

    it "composes the labels string" do
      expect(helper.labels_list(labels)).to eq('~"telemetry", ~"telemetry::reviewed"')
    end

    context "when passing a separator" do
      it "composes the labels string with the given separator" do
        expect(helper.labels_list(labels, sep: " ")).to eq('~"telemetry" ~"telemetry::reviewed"')
      end
    end

    it "returns empty string for empty array" do
      expect(helper.labels_list([])).to eq("")
    end
  end

  describe "#quick_action_label" do
    it "composes the labels string" do
      mr_labels = ["telemetry", "telemetry::reviewed"]

      expect(helper.quick_action_label(mr_labels)).to eq('/label ~"telemetry" ~"telemetry::reviewed"')
    end

    it "returns empty string for empty array" do
      expect(helper.quick_action_label([])).to eq("")
    end
  end

  describe "#has_ci_changes?" do
    context "when .gitlab/ci is changed" do
      it "returns true" do
        expect(helper).to receive(:all_changed_files).and_return(%w[migration.rb .gitlab/ci/test.yml])

        expect(helper.has_ci_changes?).to be_truthy
      end
    end

    context "when .gitlab-ci.yml is changed" do
      it "returns true" do
        expect(helper).to receive(:all_changed_files).and_return(%w[migration.rb .gitlab-ci.yml])

        expect(helper.has_ci_changes?).to be_truthy
      end
    end

    context "when neither .gitlab/ci/ or .gitlab-ci.yml is changed" do
      it "returns false" do
        expect(helper).to receive(:all_changed_files).and_return(%w[migration.rb nested/.gitlab-ci.yml])

        expect(helper.has_ci_changes?).to be_falsey
      end
    end
  end

  describe "#group_label" do
    before do
      expect(dangerfile.gitlab).to receive(:mr_labels).and_return(mr_labels)
    end

    context "when no group label is present" do
      let(:mr_labels) { %w[foo bar] }

      it "returns nil" do
        expect(helper.group_label).to be_nil
      end
    end

    context "when a group label is present" do
      let(:mr_labels) { ["foo", "group::source code", "bar"] }

      it "returns nil" do
        expect(helper.group_label).to eq("group::source code")
      end
    end
  end

  # Private methods

  describe "#diff_for_file" do
    subject { helper.__send__(:diff_for_file, "added-from-api") }

    context "with the GitLab API available" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(mr_changes_from_api["changes"])
      end

      it "returns the changes for the given file from the GitLab API" do
        is_expected.to eq(mr_changes_from_api["changes"][0]["diff"])
      end

      context "when the given file isn't modified in the API" do
        subject { helper.__send__(:diff_for_file, "not-added-from-api") }

        it "returns nil" do
          is_expected.to be_nil
        end
      end
    end

    context "with the GitLab API unavailable" do
      before do
        allow(helper).to receive(:changes_from_api).and_return(nil)
      end

      it "returns nil" do
        expect(fake_git).to receive(:diff_for_file).with("added-from-api")
        is_expected.to be_nil
      end

      context "when the file is modified in Git" do
        subject { helper.__send__(:diff_for_file, "added-from-git") }

        it "returns the changes for the given file from the danger git plugin" do
          expect(fake_git).to receive(:diff_for_file).with("added-from-git").and_return(fake_git_diff)
          is_expected.to eq(diff)
        end
      end
    end
  end

  describe "#changes_from_api" do
    context "when running locally" do
      before do
        allow(helper).to receive(:ci?).and_return(false)
      end

      it "returns nil" do
        expect(helper).not_to receive(:gitlab_helper)
        expect(helper.__send__(:changes_from_api)).to be_nil
      end
    end

    context "when running under CI" do
      it "returns changes from the API" do
        expect(helper.__send__(:changes_from_api)).to eq(mr_changes_from_api["changes"])
      end

      context "when the API raises an error" do
        it "returns nil" do
          expect(dangerfile.gitlab).to receive(:mr_changes).and_raise
          expect(helper.__send__(:changes_from_api)).to be_nil
        end
      end
    end
  end
end
