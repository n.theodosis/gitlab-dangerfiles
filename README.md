# Gitlab::Dangerfiles

The goal of this gem is to centralize Danger plugins and rules that to be used by multiple GitLab projects.

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gitlab-dangerfiles', require: false
```

And then execute:

```sh
$ bundle install
```

Or install it yourself as:

```sh
$ gem install gitlab-dangerfiles
```

## Usage

### Importing plugins and rules

In your project's `Dangerfile`, add the following two line to import the plugins and rules from this gem:

```ruby
require 'gitlab-dangerfiles'

# Get an instance of Gitlab::Dangerfiles
gitlab_dangerfiles = Gitlab::Dangerfiles::Engine.new(self)

# Import all plugins from the gem
gitlab_dangerfiles.import_plugins

# Import all rules from the gem
gitlab_dangerfiles.import_dangerfiles

# Or import a subset of rules from the gem
gitlab_dangerfiles.import_dangerfiles(rules: [:changes_size])
```

### Plugins

Danger plugins are located under `lib/danger/plugins`.

- `Danger::Helper` available in `Dangerfile`s as `helper`
- `Danger::Roulette` available in `Dangerfile`s as `roulette`

For the full documentation about the plugins, please see https://www.rubydoc.info/gems/gitlab-dangerfiles.

### Rules


Danger rules are located under `lib/danger/rules`.

#### `changes_size`

##### Available configurations

- `code_size_thresholds`: A hash of the form `{ high: 42, medium: 12 }` where
  `:high` is the lines changed threshold which triggers an error, and
  `:medium` is the lines changed threshold which triggers a warning.

#### `commit_messages`

##### Available configurations

- `max_commits_count`: The maximum number of allowed non-squashed/non-fixup commits for a given MR.
   A warning is triggered if the MR has more commits.

## Documentation

Latest documentation can be found at <https://www.rubydoc.info/gems/gitlab-dangerfiles>.

## Development

After checking out the repo, run `bin/setup` to install dependencies. Then, run `rake spec` to run the tests. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and tags, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and merge requests are welcome at https://gitlab.com/gitlab-org/gitlab-dangerfiles. This project is intended to be a safe, welcoming space for collaboration, and contributors are expected to adhere to the [code of conduct](https://gitlab.com/gitlab-org/gitlab-dangerfiles/blob/master/CODE_OF_CONDUCT.md).

## License

The gem is available as open source under the terms of the [MIT License](https://opensource.org/licenses/MIT).

## Code of Conduct

Everyone interacting in the Gitlab::Danger project's codebases, issue trackers, chat rooms and mailing lists is expected to follow the [code of conduct](https://gitlab.com/gitlab-org/gitlab-dangerfiles/blob/master/CODE_OF_CONDUCT.md).
