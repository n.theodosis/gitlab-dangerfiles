require "gitlab/dangerfiles/version"

module Gitlab
  module Dangerfiles
    RULES_DIR = File.expand_path("../danger/rules", __dir__)
    EXISTING_RULES = Dir.glob(File.join(RULES_DIR, "*")).each_with_object([]) do |path, memo|
      if File.directory?(path)
        memo << File.basename(path)
      end
    end
    LOCAL_RULES = %w[
      changes_size
      commit_messages
    ].freeze
    CI_ONLY_RULES = %w[
    ].freeze

    # This class provides utility methods to import plugins and dangerfiles easily.
    class Engine
      # @param dangerfile [Danger::Dangerfile] A +Danger::Dangerfile+ object.
      #
      # @example
      #   # In your main Dangerfile:
      #   dangerfiles = Gitlab::Dangerfiles::Engine.new(self)
      #
      # @return [Gitlab::Dangerfiles::Engine]
      def initialize(dangerfile)
        @dangerfile = dangerfile
      end

      # Import all available plugins.
      #
      # @example
      #   # In your main Dangerfile:
      #   dangerfiles = Gitlab::Dangerfiles::Engine.new(self)
      #
      #   # Import all plugins
      #   dangerfiles.import_plugins
      def import_plugins
        danger_plugin.import_plugin(File.expand_path("../danger/plugins/*.rb", __dir__))
      end

      # Import available Dangerfiles.
      #
      # @param rules [Symbol, Array<String>] Can be either +:all+ (default) to import all rules,
      #   or an array of rules.
      #   Available rules are: +changes_size+.
      #
      # @example
      #   # In your main Dangerfile:
      #   dangerfiles = Gitlab::Dangerfiles::Engine.new(self)
      #
      #   # Import all rules
      #   dangerfiles.import_dangerfiles
      #
      #   # Or import only a subset of rules
      #   dangerfiles.import_dangerfiles(rules: %w[changes_size])
      def import_dangerfiles(rules: :all)
        filtered_rules(rules).each do |rule|
          danger_plugin.import_dangerfile(path: File.join(RULES_DIR, rule))
        end
      end

      private

      attr_reader :dangerfile

      def allowed_rules
        return LOCAL_RULES unless helper_plugin.respond_to?(:ci?)

        helper_plugin.ci? ? LOCAL_RULES | CI_ONLY_RULES : LOCAL_RULES
      end

      def filtered_rules(rules)
        rules = EXISTING_RULES if rules == :all

        Array(rules).map(&:to_s) & EXISTING_RULES & allowed_rules
      end

      def danger_plugin
        @danger_plugin ||= dangerfile.plugins[Danger::DangerfileDangerPlugin]
      end

      def helper_plugin
        @helper_plugin ||= dangerfile.plugins[Danger::Helper]
      end
    end
  end
end
