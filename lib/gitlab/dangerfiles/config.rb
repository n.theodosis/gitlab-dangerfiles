# frozen_string_literal: true

module Gitlab
  module Dangerfiles
    class Config
      # @!attribute code_size_thresholds
      #   @return [{ high: Integer, medium: Integer }] a hash of the form +{ high: 42, medium: 12 }+ where +:high+ is the lines changed threshold which triggers an error, and +:medium+ is the lines changed threshold which triggers a warning. Also, see +DEFAULT_CHANGES_SIZE_THRESHOLDS+ for the format of the hash.
      attr_accessor :code_size_thresholds

      # @!attribute max_commits_count
      #   @return [Integer] the maximum number of allowed non-squashed/non-fixup commits for a given MR. A warning is triggered if the MR has more commits.
      attr_accessor :max_commits_count

      DEFAULT_CHANGES_SIZE_THRESHOLDS = { high: 2_000, medium: 500 }.freeze
      DEFAULT_COMMIT_MESSAGES_MAX_COMMITS_COUNT = 10

      def initialize
        @code_size_thresholds = DEFAULT_CHANGES_SIZE_THRESHOLDS
        @max_commits_count = DEFAULT_COMMIT_MESSAGES_MAX_COMMITS_COUNT
      end
    end
  end
end
