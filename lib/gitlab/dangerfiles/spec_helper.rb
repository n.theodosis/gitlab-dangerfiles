require "danger"
require "gitlab/dangerfiles/changes"

module DangerSpecHelper
  # These functions are a subset of https://github.com/danger/danger/blob/master/spec/spec_helper.rb
  # If you are expanding these files, see if it's already been done ^.

  # A silent version of the user interface
  def self.testing_ui
    Cork::Board.new(silent: true)
  end

  # Example environment (ENV) that would come from
  # running a PR on TravisCI
  def self.testing_env
    {
      "GITLAB_CI" => "true",
      "DANGER_GITLAB_HOST" => "gitlab.example.com",
      "CI_MERGE_REQUEST_IID" => 28_493,
      "DANGER_GITLAB_API_TOKEN" => "123sbdq54erfsd3422gdfio",
    }
  end

  # A stubbed out Dangerfile for use in tests
  def self.testing_dangerfile
    env = Danger::EnvironmentManager.new(testing_env)
    Danger::Dangerfile.new(env, testing_ui)
  end

  def self.fake_danger
    Class.new do
      attr_reader :git, :gitlab, :helper

      # rubocop:disable Gitlab/ModuleWithInstanceVariables
      def initialize(git: nil, gitlab: nil, helper: nil)
        @git = git
        @gitlab = gitlab
        @helper = helper
      end

      # rubocop:enable Gitlab/ModuleWithInstanceVariables
    end
  end
end

RSpec.shared_context "with dangerfile" do
  let(:dangerfile) { DangerSpecHelper.testing_dangerfile }
  let(:added_files) { %w[added-from-git] }
  let(:modified_files) { %w[modified-from-git] }
  let(:deleted_files) { %w[deleted-from-git] }
  let(:renamed_before_file) { "renamed_before-from-git" }
  let(:renamed_after_file) { "renamed_after-from-git" }
  let(:renamed_files) { [{ before: renamed_before_file, after: renamed_after_file }] }
  let(:change_class) { Gitlab::Dangerfiles::Change }
  let(:changes_class) { Gitlab::Dangerfiles::Changes }
  let(:changes) { changes_class.new([]) }
  let(:mr_title) { "Fake Title" }
  let(:mr_labels) { [] }
  let(:mr_changes_from_api) do
    {
      "changes" => [
        {
          "old_path" => "added-from-api",
          "new_path" => "added-from-api",
          "a_mode" => "100644",
          "b_mode" => "100644",
          "new_file" => true,
          "renamed_file" => false,
          "deleted_file" => false,
          "diff" => "@@ -49,6 +49,14 @@\n- vendor/ruby/\n     policy: pull\n \n+.danger-review-cache:\n",
        },
        {
          "old_path" => "modified-from-api",
          "new_path" => "modified-from-api",
          "a_mode" => "100644",
          "b_mode" => "100644",
          "new_file" => false,
          "renamed_file" => false,
          "deleted_file" => false,
          "diff" => "@@ -49,6 +49,14 @@\n- vendor/ruby/\n     policy: pull\n \n+.danger-review-cache:\n",
        },
        {
          "old_path" => "renamed_before-from-api",
          "new_path" => "renamed_after-from-api",
          "a_mode" => "100644",
          "b_mode" => "100644",
          "new_file" => false,
          "renamed_file" => true,
          "deleted_file" => false,
          "diff" => "@@ -49,6 +49,14 @@\n- vendor/ruby/\n     policy: pull\n \n+.danger-review-cache:\n",
        },
        {
          "old_path" => "deleted-from-api",
          "new_path" => "deleted-from-api",
          "a_mode" => "100644",
          "b_mode" => "100644",
          "new_file" => false,
          "renamed_file" => false,
          "deleted_file" => true,
          "diff" => "@@ -49,6 +49,14 @@\n- vendor/ruby/\n     policy: pull\n \n+.danger-review-cache:\n",
        },
      ],
    }
  end

  let(:fake_git) { double("fake-git", added_files: added_files, modified_files: modified_files, deleted_files: deleted_files, renamed_files: renamed_files) }
  let(:fake_helper) { double("fake-helper", changes: changes, added_files: added_files, modified_files: modified_files, deleted_files: deleted_files, renamed_files: renamed_files, mr_iid: 1234, mr_title: mr_title, mr_labels: mr_labels) }

  before do
    allow(dangerfile).to receive(:git).and_return(fake_git)
  end
end
